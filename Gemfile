source 'https://rubygems.org'

ruby '1.9.3'
gem 'rake',                 '0.9.2.2'
gem 'rails',                '3.2.14'

group :assets do
  gem 'sass-rails'
  gem 'bootstrap-sass'
  gem 'coffee-rails'
  gem 'uglifier'
  gem 'compass-rails'
  # gem 'jquery-rails'
  gem 'cloudfoundry-jquery-rails'
  gem 'jquery-ui-rails'
  gem 'jquery-modal-rails'
  gem 'highcharts-rails',   '~> 2.3.5'
  gem 'jquery-qtip2-rails'
  gem 'asset_sync'
  gem 'jquery-simplecolorpicker-rails', '~> 0.4.0'
  gem 'colorbox-rails',     '~> 0.1.0'
end

gem 'pg',                   '0.12.2'
gem 'gritter'
gem 'bcrypt-ruby',          '~> 3.0.0'
gem 'settingslogic'
gem 'resque',               require: 'resque/server'
gem 'resque-scheduler',     '~> 2.0.0', :require => 'resque_scheduler'
gem 'resque_mailer',        '~> 2.2.1'
gem 'haml-rails',           '~> 0.3.4'
gem 'simple_form'
gem 'rabl',                 '~> 0.8.0'
gem 'yajl-ruby',            require: 'yajl'
gem 'cancan'
gem 'mini_magick'
gem 'fog'
gem 'devise',               '2.0.4'
gem 'awesome_nested_set',   '~> 2.1.3'
gem 'carrierwave',          '~> 0.6.2'
gem 'remotipart',           '~> 1.0'
gem 'ckeditor',             '~> 4.0.2'
gem 'kaminari',             '~> 0.14.1'
gem 'memcachier'
gem 'dalli'
gem 'font-awesome-rails',   '~> 3.1.1.2'
gem 'pdfcrowd',             '~> 2.4.0'

group :development, :test do
  gem 'hirb'
  gem 'awesome_print'
  # gem 'debugger',           '~> 1.6.2'

  gem 'rspec-rails',        '>= 2.0.1'
  gem 'capybara'
  gem 'shoulda-matchers',   '~> 1.4.2'
  gem 'factory_girl_rails', '~> 3.0'
  gem 'faker'
  gem 'database_cleaner',   '>= 0.6.7'
  gem 'launchy',            '>= 0.4.0'
  gem 'timecop'
end

group :test do
  gem 'simplecov',          :require => false
end

group :development do
  gem 'bullet'
  gem 'sextant'

  gem 'binding_of_caller'
  gem 'better_errors'

  gem 'meta_request',       '0.2.7' # required by rails_panel chrome plugin

  gem 'letter_opener'
end

group :production, :staging do
  gem 'unicorn'
  gem 'newrelic_rpm',       '~> 3.6.2.96'
end

# gem 'pgbackups-archive',  '~> 0.2.0'
gem 'airbrake'

# Deploy with Capistrano
# gem 'capistrano'
